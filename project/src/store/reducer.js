import {ActionType} from './acton';
import promo from '../mocks/promo';
import {AuthStatus} from '../const';

const initialState = {
  films: [],
  promo: promo,
  genre: 0,
  pageCount: 1,
  authStatus: AuthStatus.UNKNOWN,
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.CHANGE_FILTER_GENRE:
      return {
        ...state,
        genre: action.payload,
      };
    case ActionType.INCREASE_PAGE_COUNT:
      return {
        ...state,
        pageCount: action.payload + 1,
      };
    case ActionType.RESET_PAGE_COUNT:
      return {
        ...state,
        pageCount: initialState.pageCount,
      };
    case ActionType.LOAD_FILMS:
      return {
        ...state,
        films: action.payload,
        isLoading: initialState.isLoading,
      };
    case ActionType.SET_AUTH_STATUS:
      return {
        ...state,
        authStatus: action.payload,
      };
    case ActionType.LOGOUT:
      return {
        ...state,
        authStatus: AuthStatus.NOT_AUTH,
      };
    case ActionType.SET_IS_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    default:
      return state;
  }
};

export {reducer};
