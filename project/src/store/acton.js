export const ActionType = {
  CHANGE_FILTER_GENRE: 'main/changeFilterGenre',
  INCREASE_PAGE_COUNT: 'main/increasePageCount',
  RESET_PAGE_COUNT: 'main/resetPageCount',
  LOAD_FILMS: 'data/loadFilms',
  SET_AUTH_STATUS: 'user/setAuthStatus',
  LOGOUT: 'user/logout',
  SET_IS_LOADING: 'data/setIsLoading',
};

export const ActionCreator = {
  changeFilterGenre: (genre) => ({
    type: ActionType.CHANGE_FILTER_GENRE,
    payload: genre,
  }),
  increasePageCount: (count) => ({
    type: ActionType.INCREASE_PAGE_COUNT,
    payload: count,
  }),
  resetPageCount: () => ({
    type: ActionType.RESET_PAGE_COUNT,
  }),
  loadFilms: (films) => ({
    type: ActionType.LOAD_FILMS,
    payload: films,
  }),
  setAuthStatus: (status) => ({
    type: ActionType.SET_AUTH_STATUS,
    payload: status,
  }),
  logout: () => ({
    type: ActionType.LOGOUT,
  }),
  setIsLoading: () => ({
    type: ActionType.SET_IS_LOADING,
  }),
};
