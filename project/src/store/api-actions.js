import {APIRoute, APIStatus, AuthStatus} from '../const';
import {ActionCreator} from './acton';
import {formatJSON} from '../utils/format-json';

export const getFilms = () => (dispatch, _getState, api) => {
  api.get(APIRoute.FILMS)
    .then(({data}) => dispatch(ActionCreator.loadFilms(formatJSON(data))));
};

export const checkAuth = () => (dispatch, _getState, api) => (
  api.get(APIRoute.LOGIN)
    .then(() => dispatch(ActionCreator.setAuthStatus(AuthStatus.AUTH)))
    .catch((error) => {
      const {response} = error;

      if (response.status === APIStatus.UNAUTHORIZED) {
        dispatch(ActionCreator.setAuthStatus(AuthStatus.NOT_AUTH));
      }
    })
);

export const login = ({email, password}) => (dispatch, _getState, api) => (
  api.post(APIRoute.LOGIN, {email, password})
    .then(({data}) => localStorage.setItem('token', data.token))
    .then(() => dispatch(ActionCreator.setAuthStatus(AuthStatus.AUTH)))
);

// export const logout = () => (dispatch, _getState, api) => (
//   api.delete(APIRoute.LOGOUT)
//     .then(() => localStorage.removeItem('token'))
//     .then(() => dispatch(ActionCreator.logout()))
// );
