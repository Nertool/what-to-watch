export const AppRoute = {
  ROOT: '/',
  LOGIN: '/login',
  MY_LIST: '/mylist',
  FILM: '/films',
  ADD_REVIEW: '/review',
  PLAYER: '/player',
};

export const MAX_COUNT_FILMS_IN_PAGE = 8;

export const INITIAL_FILM_GENRE = 'All genres';

export const tabsForFilmScreen = ['Overview', 'Details', 'Reviews'];

export const FilmLevel = {
  BAD: 'Bad',
  NORMAL: 'Normal',
  GOOD: 'Good',
  VERY_GOOD: 'Very good',
  AWESOME: 'Awesome',
};

export const AuthStatus = {
  AUTH: 'AUTH',
  NOT_AUTH: 'NOT_AUTH',
  UNKNOWN: 'UNKNOWN',
};

export const APIRoute = {
  FILMS: '/films',
  LOGIN: '/login',
};

export const APIStatus = {
  UNAUTHORIZED: 401,
};
