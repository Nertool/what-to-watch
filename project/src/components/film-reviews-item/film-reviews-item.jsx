import React from 'react';
import commentProp from '../../comment.prop';
import formatDate from '../../utils/format-date';

function FilmReviewsItem(props) {
  const {review} = props;
  return (
    <div className="review">
      <blockquote className="review__quote">
        <p className="review__text">{ review.comment }</p>

        <footer className="review__details">
          <cite className="review__author">{ review.user.name }</cite>
          <time className="review__date" dateTime="2016-12-24">{ formatDate(review.date) }</time>
        </footer>
      </blockquote>

      <div className="review__rating">{ review.rating }</div>
    </div>
  );
}

FilmReviewsItem.propTypes = {
  review: commentProp,
};

export default FilmReviewsItem;
