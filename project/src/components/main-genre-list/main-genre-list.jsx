import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import MainGenreItem from '../main-genre-item/main-genre-item';
import {connect} from 'react-redux';
import {ActionCreator} from '../../store/acton';

function MainGenreList (props) {
  const {genre, genreList, changeFilterGenre, resetPageCount} = props;
  const [activeGenre, setActiveGenre] = useState(genreList[genre]);

  useEffect(() => {
    setActiveGenre(genreList[genre]);
  }, [genre, genreList]);

  const changeActiveHandler = (evt, indexGenre) => {
    evt.preventDefault();
    changeFilterGenre(indexGenre);
    resetPageCount();
  };

  return (
    <ul className="catalog__genres-list">

      { genreList.map((name, index) => <MainGenreItem key={name} name={name} activeGenre={activeGenre} changeActiveHandler={changeActiveHandler} index={index}/>) }

    </ul>
  );
}

MainGenreList.propTypes = {
  genre: PropTypes.number.isRequired,
  genreList: PropTypes.array.isRequired,
  changeFilterGenre: PropTypes.func.isRequired,
  resetPageCount: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  changeFilterGenre(index) {
    dispatch(ActionCreator.changeFilterGenre(index));
  },
  resetPageCount () {
    dispatch(ActionCreator.resetPageCount());
  },
});

export {MainGenreList};
export default connect(null, mapDispatchToProps)(MainGenreList);
