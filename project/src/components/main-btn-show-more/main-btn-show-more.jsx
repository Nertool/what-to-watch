import React from 'react';
import PropTypes from 'prop-types';

function MainBtnShowMore(props) {
  const {showMoreHandler} = props;

  return (
    <div className="catalog__more">
      <button className="catalog__button" type="button" onClick={showMoreHandler}>Show more</button>
    </div>
  );
}

MainBtnShowMore.propTypes = {
  showMoreHandler: PropTypes.func.isRequired,
};

export default MainBtnShowMore;
