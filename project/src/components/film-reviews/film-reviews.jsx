import React from 'react';
import PropTypes from 'prop-types';
import commentProp from '../../comment.prop';
import FilmReviewsItem from '../film-reviews-item/film-reviews-item';

function FilmReviews(props) {
  const {reviews} = props;
  const renderReviewList = (review, bool1, bool2 = true) => {
    if (bool1 && bool2) {
      return <FilmReviewsItem key={review.id} review={review} />;
    } else {
      return false;
    }
  };

  return (
    <div className="film-card__reviews film-card__row">
      <div className="film-card__reviews-col">
        { reviews.map((review, index) => renderReviewList(review, index < 3)) }
      </div>
      {
        reviews.length > 3 &&
        <div className="film-card__reviews-col">
          { reviews.map((review, index) => renderReviewList(review, index > 2, index < 6)) }
        </div>
      }
    </div>
  );
}

FilmReviews.propTypes = {
  reviews: PropTypes.arrayOf(commentProp),
};

export default FilmReviews;
