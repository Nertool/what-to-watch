import React from 'react';
import filmProp from '../../film.prop';
import {FilmLevel} from '../../const';

function FilmOverview(props) {
  const {film} = props;

  const getLevelFilm = (rating) => {
    const num = Math.round(rating);

    if (num < 3) {
      return FilmLevel.BAD;
    } else if (num >= 3 && num < 5) {
      return FilmLevel.NORMAL;
    } else if (num >= 5 && num < 8) {
      return FilmLevel.GOOD;
    } else if (num >= 8 && num < 10) {
      return FilmLevel.VERY_GOOD;
    } else if (num === 10) {
      return FilmLevel.AWESOME;
    }
  };

  return (
    <React.Fragment>
      <div className="film-rating">
        <div className="film-rating__score">{ film.rating }</div>
        <p className="film-rating__meta">
          <span className="film-rating__level">{ getLevelFilm(film.rating) }</span>
          <span className="film-rating__count">{ film.scoresCount } ratings</span>
        </p>
      </div>

      <div className="film-card__text">
        <p>{ film.description }</p>
        <p className="film-card__director"><strong>Director: { film.director }</strong></p>
        <p className="film-card__starring"><strong>Starring: { film.starring.join(', ') }</strong></p>
      </div>
    </React.Fragment>
  );
}

FilmOverview.propTypes = {
  film: filmProp,
};

export default FilmOverview;
