import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {AppRoute} from '../../const';

function FormReview(props) {
  const {filmId} = props;
  const [ratingValue, setRatingValue] = useState(5);
  const [commentValue, setCommentValue] = useState('');
  const history = useHistory();
  const submitHandler = (evt) => {
    evt.preventDefault();
    setCommentValue('');
    setRatingValue(5);
    history.push(`${AppRoute.FILM}/${filmId}`);
  };
  const changeCommentHandler = (evt) => {
    setCommentValue(evt.target.value);
  };
  const changeRatingHandler = (evt) => {
    setRatingValue(evt.target.value);
  };
  const getCountStarsRating = () => {
    const countStarsRating = [];

    for (let count = 1; count <= 10; count++) {
      countStarsRating.unshift(count);
    }

    return countStarsRating;
  };

  return (
    <div className="add-review">
      <form action="#" className="add-review__form" onSubmit={submitHandler}>
        <div className="rating">
          <div className="rating__stars">
            {
              getCountStarsRating().map((count) => (
                <React.Fragment key={count}>
                  <input className="rating__input" id={`star-${count}`} type="radio" name="rating" value={`${count}`} defaultChecked={count === ratingValue} onChange={changeRatingHandler}/>
                  <label className="rating__label" htmlFor={`star-${count}`}>Rating { count }</label>
                </React.Fragment>
              ))
            }
          </div>
        </div>

        <div className="add-review__text">
          <textarea className="add-review__textarea" name="review-text" id="review-text" placeholder="Review text" onChange={changeCommentHandler} value={commentValue} maxLength='400'></textarea>
          <div className="add-review__submit">
            <button className="add-review__btn" type="submit" disabled={commentValue.length <= 50}>Post</button>
          </div>
          <div>{ JSON.stringify({filmId, ratingValue, commentValue}) }</div>
        </div>
      </form>
    </div>
  );
}

FormReview.propTypes = {
  filmId: PropTypes.number.isRequired,
};

export default FormReview;
