import React, {useRef} from 'react';
import filmProp from '../../film.prop';
import {Link} from 'react-router-dom';
import {AppRoute} from '../../const';

function FilmCard(props) {
  const {film} = props;
  const video = useRef(null);
  let timeout = null;

  const onPreviewHandler = () => {
    timeout = setTimeout(() => {
      video.current.play();
    }, 1000);
  };

  const offPreviewHandler = () => {
    clearTimeout(timeout);
    video.current.load();
  };

  return (
    <article className="small-film-card catalog__films-card" onMouseEnter={onPreviewHandler} onMouseLeave={offPreviewHandler} onClick={offPreviewHandler}>
      <div className="small-film-card__image">
        <video ref={video} poster={film.previewImage} src={film.previewVideoLink} width="280" height="175" muted />
      </div>
      <h3 className="small-film-card__title">
        <Link className="small-film-card__link" to={`${AppRoute.FILM}/${film.id}`}>{film.name}</Link>
      </h3>
    </article>
  );
}

FilmCard.propTypes = {
  film: filmProp,
};

export default FilmCard;
