import React from 'react';
import PropTypes from 'prop-types';
import {tabsForFilmScreen} from '../../const';
import FilmTabsNavItem from '../film-tabs-nav-item/film-tabs-nav-item';

function FilmTabsNav(props) {
  const {activeTabIndex, changeTabHandler} = props;

  return (
    <div className="film-card__desc">
      <nav className="film-nav film-card__nav">
        <ul className="film-nav__list">

          { tabsForFilmScreen.map((tab, index) => <FilmTabsNavItem key={tab} tabIndex={index} tab={tab} isActive={activeTabIndex === index} changeTabHandler={changeTabHandler} />)}

        </ul>
      </nav>
    </div>
  );
}

FilmTabsNav.propTypes = {
  activeTabIndex: PropTypes.number.isRequired,
  changeTabHandler: PropTypes.func.isRequired,
};

export default FilmTabsNav;
