import React from 'react';
import PropTypes from 'prop-types';

function MainGenreItem(props) {
  const {name, activeGenre, changeActiveHandler, index} = props;

  return (
    <li className={`catalog__genres-item ${name === activeGenre ? 'catalog__genres-item--active' : ''}`}>
      <a href="#s" className="catalog__genres-link" onClick={(evt) => changeActiveHandler(evt, index)}>{ name }</a>
    </li>
  );
}

MainGenreItem.propTypes = {
  name: PropTypes.string.isRequired,
  activeGenre: PropTypes.string.isRequired,
  changeActiveHandler: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};

export default MainGenreItem;
