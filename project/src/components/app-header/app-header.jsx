import React from 'react';
import PropTypes from 'prop-types';
import AppLogo from '../app-logo/app-logo';
import AppUser from '../app-user/app-user';

function AppHeader(props) {
  const {className = '', user = true} = props;
  return (
    <header className={`page-header ${className}`}>
      <AppLogo />
      { props.children }
      { user && <AppUser />}
    </header>
  );
}

AppHeader.propTypes = {
  className: PropTypes.string,
  user: PropTypes.bool,
  children: PropTypes.object,
};

export default AppHeader;
