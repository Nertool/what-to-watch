import React from 'react';
import FilmCard from '../film-card/film-card';
import PropTypes from 'prop-types';
import filmProp from '../../film.prop';
import {MAX_COUNT_FILMS_IN_PAGE} from '../../const';

function FilmList (props) {
  const {films, pageCount} = props;

  return (
    <div className="catalog__films-list">

      {
        films.map((film, index) => {
          if (index < MAX_COUNT_FILMS_IN_PAGE * pageCount) {
            return <FilmCard key={film.id} film={film} />;
          } else {
            return false;
          }
        })
      }

    </div>
  );
}

FilmList.propTypes = {
  films: PropTypes.arrayOf(filmProp),
  pageCount: PropTypes.number.isRequired,
};

export default FilmList;
