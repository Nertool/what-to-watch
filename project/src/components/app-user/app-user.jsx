import React from 'react';
import {Link} from 'react-router-dom';
import {AppRoute, AuthStatus} from '../../const';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

function AppUser(props) {
  const {authStatus} = props;

  if (authStatus === AuthStatus.NOT_AUTH) {
    return (
      <div className="user-block">
        <Link to={AppRoute.LOGIN} className="user-block__link">Sign in</Link>
      </div>
    );
  }

  return (
    <ul className="user-block">
      <li className="user-block__item">
        <div className="user-block__avatar">
          <Link to={AppRoute.MY_LIST}>
            <img src="img/avatar.jpg" alt="User avatar" width="63" height="63"/>
          </Link>
        </div>
      </li>
      <li className="user-block__item">
        <a className="user-block__link" href='#s'>Sign out</a>
      </li>
    </ul>
  );
}

AppUser.propTypes = {
  authStatus: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  authStatus: state.authStatus,
});

const mapDispatchToProps = (dispatch) => ({

});

export {AppUser};
export default connect(mapStateToProps, mapDispatchToProps)(AppUser);
