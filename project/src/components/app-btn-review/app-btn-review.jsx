import React, {useState} from 'react';
import filmProp from '../../film.prop';

function AppBtnReview(props) {
  const {film} = props;
  const [isFavorite, setIsFavorite] = useState(film.isFavorite);
  const toggleFavoriteHandler = (status) => {
    setIsFavorite(!status);
  };

  return (
    <button className="btn btn--list film-card__button" type="button" onClick={() => toggleFavoriteHandler(isFavorite)}>
      <svg viewBox="0 0 19 20" width="19" height="20">
        <use xlinkHref={ isFavorite ? '#in-list' : '#add'}></use>
      </svg>
      <span>My list</span>
    </button>
  );
}

AppBtnReview.propTypes = {
  film: filmProp,
};

export default AppBtnReview;
