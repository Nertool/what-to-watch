import React from 'react';
import PropTypes from 'prop-types';

function FilmTabsNavItem(props) {
  const {tabIndex, changeTabHandler, isActive, tab} = props;

  return (
    <li className={`film-nav__item ${ isActive ? 'film-nav__item--active' : ''}`} onClick={(evt) => {changeTabHandler(evt, tabIndex);}}>
      <a href="#s" className="film-nav__link">{ tab }</a>
    </li>
  );
}

FilmTabsNavItem.propTypes = {
  tabIndex: PropTypes.number.isRequired,
  tab: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  changeTabHandler: PropTypes.func.isRequired,
};

export default FilmTabsNavItem;
