import React from 'react';
import filmProp from '../../film.prop';
import {formatTime} from '../../utils/format-time';

function FilmDetails(props) {
  const {film} = props;
  const {hours, minutes} = formatTime(film.runTime);

  return (
    <div className="film-card__text film-card__row">
      <div className="film-card__text-col">
        <p className="film-card__details-item">
          <strong className="film-card__details-name">Director</strong>
          <span className="film-card__details-value">{ film.director }</span>
        </p>
        <p className="film-card__details-item">
          <strong className="film-card__details-name">Starring</strong>
          <span className="film-card__details-value">

            { film.starring.map((star, index) => {
              if (index !== film.starring.length - 1) {
                return <React.Fragment key={star}>{star}, <br/></React.Fragment>;
              } else {
                return star;
              }
            })}

          </span>
        </p>
      </div>

      <div className="film-card__text-col">
        <p className="film-card__details-item">
          <strong className="film-card__details-name">Run Time</strong>
          <span className="film-card__details-value">{ `${hours}h ${minutes}m` }</span>
        </p>
        <p className="film-card__details-item">
          <strong className="film-card__details-name">Genre</strong>
          <span className="film-card__details-value">{ film.genre }</span>
        </p>
        <p className="film-card__details-item">
          <strong className="film-card__details-name">Released</strong>
          <span className="film-card__details-value">{ film.released }</span>
        </p>
      </div>
    </div>
  );
}

FilmDetails.propTypes = {
  film: filmProp,
};

export default FilmDetails;
