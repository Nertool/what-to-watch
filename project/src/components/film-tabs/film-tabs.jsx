import React, {useState} from 'react';
import PropTypes from 'prop-types';
import FilmOverview from '../film-overview/film-overview';
import FilmDetails from '../film-details/film-details';
import FilmReviews from '../film-reviews/film-reviews';
import FilmTabsNav from '../film-tabs-nav/film-tabs-nav';
import filmProp from '../../film.prop';
import commentProp from '../../comment.prop';

function FilmTabs(props) {
  const {film, comments} = props;
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const changeTabHandler = (evt, index) => {
    evt.preventDefault();
    setActiveTabIndex(index);
  };
  const getCurrentTabContent = (index) => {
    switch (index) {
      case 0:
        return <FilmOverview film={film} />;
      case 1:
        return <FilmDetails film={film} />;
      case 2:
        return <FilmReviews reviews={comments} />;
      default:
        return false;
    }
  };

  return (
    <div className="film-card__desc">

      <FilmTabsNav activeTabIndex={activeTabIndex} changeTabHandler={changeTabHandler} />

      { getCurrentTabContent(activeTabIndex) }

    </div>
  );
}

FilmTabs.propTypes = {
  film: filmProp,
  comments: PropTypes.arrayOf(commentProp),
};

export default FilmTabs;
