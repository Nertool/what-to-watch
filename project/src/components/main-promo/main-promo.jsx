import React from 'react';
import filmProp from '../../film.prop';
import AppHeader from '../app-header/app-header';
import AppBtnPlay from '../app-btn-play/app-btn-play';
import AppBtnReview from '../app-btn-review/app-btn-review';

function MainPromo (props) {
  const {promo} = props;

  return (
    <section className="film-card">
      <div className="film-card__bg">
        <img src={promo.backgroundImage} alt={promo.name}/>
      </div>

      <h1 className="visually-hidden">WTW</h1>

      <AppHeader className='film-card__head' />

      <div className="film-card__wrap">
        <div className="film-card__info">
          <div className="film-card__poster">
            <img src={promo.posterImage} alt={promo.name} width="218" height="327"/>
          </div>

          <div className="film-card__desc">
            <h2 className="film-card__title">{promo.name}</h2>
            <p className="film-card__meta">
              <span className="film-card__genre">{promo.genre}</span>
              <span className="film-card__year">{promo.released}</span>
            </p>

            <div className="film-card__buttons">

              <AppBtnPlay film={promo} />
              <AppBtnReview film={promo} />

            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

MainPromo.propTypes = {
  promo: filmProp,
};

export default MainPromo;
