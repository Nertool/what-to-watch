import React from 'react';
import PropTypes from 'prop-types';
import filmProp from '../../film.prop';
import FilmCard from '../film-card/film-card';

function FilmSimilar(props) {
  const {similar} = props;
  return (
    <section className="catalog catalog--like-this">
      <h2 className="catalog__title">More like this</h2>
      <div className="catalog__films-list">

        { similar.map((film, index) => {
          if (index < 4) {
            return <FilmCard key={film.id} film={film} />;
          } else {
            return false;
          }
        }) }

      </div>
    </section>
  );
}

FilmSimilar.propTypes = {
  similar: PropTypes.arrayOf(filmProp),
};

export default FilmSimilar;
