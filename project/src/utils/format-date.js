export default function formatDate(value, format = 'month-yyyy') {
  const options = {};

  if (format === 'month-yyyy') {
    options.year = 'numeric';
    options.month = 'long';
    options.day = 'numeric';

    return new Intl.DateTimeFormat('en-US', options).format(new Date(value));
  }
}
