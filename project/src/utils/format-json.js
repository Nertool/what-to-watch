export const formatJSON = (data) => {
  if (data instanceof Array) {
    return data.map(formatJSON);
  } else if (data instanceof Object) {
    return Object.fromEntries(
      Object.entries(data).map((v) => [
        v[0].replace(/_+(.)/g, (match, word) => word.toUpperCase()),
        formatJSON(v[1]),
      ]),
    );
  } else {
    return data;
  }
};
