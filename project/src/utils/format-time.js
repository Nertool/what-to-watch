export const formatTime = (minutes) => {
  const time = {
    hours: 0,
    minutes: 0,
  };

  time.hours = Math.floor(minutes / 60);
  time.minutes = minutes - (time.hours * 60);

  return time;
};
