import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import film from './mocks/film';
import comments from './mocks/comments';
import similar from './mocks/similar';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {reducer} from './store/reducer';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createAPI} from './services/api';

const api = createAPI();

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk.withExtraArgument(api))));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App film={film} comments={comments} similar={similar} />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'));
