import React, {useEffect, useState} from 'react';
import AppHeader from '../../components/app-header/app-header';
import AppFooter from '../../components/app-footer/app-footer';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {login} from '../../store/api-actions';
import {AppRoute, AuthStatus} from '../../const';
import browserHistory from '../../browser-history';

function Login (props) {
  const {onSubmit, authStatus} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const inputHandler = (evt) => {
    const type = evt.target.type;
    const value = evt.target.value;

    switch (type) {
      case 'email':
        setEmail(value);
        break;
      case 'password':
        setPassword(value);
        break;
      default:
        break;
    }
  };

  const isValidForm = () => !!email.length && !!password.length;

  const submitHandler = (evt) => {
    evt.preventDefault();
    onSubmit({email, password});
  };

  useEffect(() => {
    if (authStatus === AuthStatus.AUTH) {
      browserHistory.push(AppRoute.ROOT);
    }
  }, [authStatus]);

  return (
    <div className="user-page">
      <AppHeader className='user-page__head' user={false}>
        <h1 className="page-title user-page__title">Sign in</h1>
      </AppHeader>

      <div className="sign-in user-page__content">
        <form action="#" className="sign-in__form" onSubmit={submitHandler}>
          <div className="sign-in__fields">
            <div className="sign-in__field">
              <input className="sign-in__input" type="email" placeholder="Email address" name="user-email" id="user-email" onInput={inputHandler} value={email} />
              <label className="sign-in__label visually-hidden" htmlFor="user-email">Email address</label>
            </div>
            <div className="sign-in__field">
              <input className="sign-in__input" type="password" placeholder="Password" name="user-password" id="user-password" onInput={inputHandler} value={password} />
              <label className="sign-in__label visually-hidden" htmlFor="user-password">Password</label>
            </div>
          </div>
          <div className="sign-in__submit">
            <button className="sign-in__btn" type="submit" disabled={!isValidForm()}>Sign in</button>
          </div>
        </form>
      </div>

      <AppFooter />
    </div>
  );
}

Login.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  authStatus: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  authStatus: state.authStatus,
});

const mapDispatchToProps = (dispatch) => ({
  onSubmit (data) {
    dispatch(login(data));
  },
});

export {Login};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
