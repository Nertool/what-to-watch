import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import filmProp from '../../film.prop';
import {INITIAL_FILM_GENRE, MAX_COUNT_FILMS_IN_PAGE} from '../../const';
import AppFooter from '../../components/app-footer/app-footer';
import FilmList from '../../components/film-list/film-list';
import MainPromo from '../../components/main-promo/main-promo';
import MainGenreList from '../../components/main-genre-list/main-genre-list';
import MainBtnShowMore from '../../components/main-btn-show-more/main-btn-show-more';
import {connect} from 'react-redux';
import {ActionCreator} from '../../store/acton';
import AppLoader from '../../components/app-loader/app-loader';
import {getFilms} from '../../store/api-actions';

function Main(props) {
  const {films, promo, genre, pageCount, increasePageCount, setIsLoading, isLoading, getFilmList} = props;
  const [filterFilmList, setFilterFilmList] = useState(films);
  const [genreList, setGenreList] = useState([INITIAL_FILM_GENRE]);

  useEffect(() => {
    setIsLoading();
    getFilmList();
  }, []);

  useEffect(() => {
    setGenreList(genreList.concat(getGenres()));
  }, [films]);

  function getGenres() {
    const genres = [];

    films.forEach((film) => {
      if (!genres.includes(film.genre) && genres.length < 10) {
        genres.push(film.genre);
      }
    });

    return genres;
  }

  const maxPageCount = Math.ceil(filterFilmList.length / MAX_COUNT_FILMS_IN_PAGE);
  const showMoreHandler = () => {
    increasePageCount(pageCount);
  };

  useEffect(() => {
    const genreName = genreList[genre];

    if (genreName === INITIAL_FILM_GENRE) {
      setFilterFilmList(films);
    } else {
      setFilterFilmList(films.filter((film) => film.genre === genreName));
    }
  }, [films, genre]);

  if (isLoading) {
    return <AppLoader />;
  }

  return (
    <React.Fragment>
      <MainPromo promo={promo} />

      <div className="page-content">
        <section className="catalog">
          <h2 className="catalog__title visually-hidden">Catalog</h2>

          <MainGenreList genre={genre} genreList={genreList} />
          <FilmList films={filterFilmList} pageCount={pageCount} />

          { pageCount < maxPageCount && <MainBtnShowMore showMoreHandler={showMoreHandler} /> }
        </section>

        <AppFooter />
      </div>
    </React.Fragment>
  );
}

Main.propTypes = {
  films: PropTypes.arrayOf(filmProp),
  promo: filmProp,
  genre: PropTypes.number.isRequired,
  pageCount: PropTypes.number.isRequired,
  increasePageCount: PropTypes.func.isRequired,
  setIsLoading: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  getFilmList: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  films: state.films,
  promo: state.promo,
  genre: state.genre,
  pageCount: state.pageCount,
  isLoading: state.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  increasePageCount (count) {
    dispatch(ActionCreator.increasePageCount(count));
  },
  setIsLoading () {
    dispatch(ActionCreator.setIsLoading());
  },
  getFilmList () {
    dispatch(getFilms());
  },
});

export {Main};
export default connect(mapStateToProps, mapDispatchToProps)(Main);
