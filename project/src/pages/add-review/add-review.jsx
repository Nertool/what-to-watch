import React from 'react';
import {Link} from 'react-router-dom';
import {AppRoute} from '../../const';
import filmProp from '../../film.prop';
import AppHeader from '../../components/app-header/app-header';
import FormReview from '../../components/form-review/form-review';

function AddReview(props) {
  const {film} = props;
  return (
    <section className="film-card film-card--full">
      <div className="film-card__header">
        <div className="film-card__bg">
          <img src={film.backgroundImage} alt={film.name}/>
        </div>

        <h1 className="visually-hidden">WTW</h1>

        <AppHeader>
          <nav className="breadcrumbs">
            <ul className="breadcrumbs__list">
              <li className="breadcrumbs__item">
                <Link to={`${AppRoute.FILM}/${film.id}`} className="breadcrumbs__link">{ film.name }</Link>
              </li>
              <li className="breadcrumbs__item">
                <span className="breadcrumbs__link">Add review</span>
              </li>
            </ul>
          </nav>
        </AppHeader>

        <div className="film-card__poster film-card__poster--small">
          <img src={film.posterImage} alt={film.name} width="218" height="327"/>
        </div>
      </div>

      <FormReview filmId={film.id} />

    </section>
  );
}

AddReview.propTypes = {
  film: filmProp,
};

export default AddReview;
