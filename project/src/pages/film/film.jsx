import React from 'react';
import filmProp from '../../film.prop';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {AppRoute} from '../../const';
import AppHeader from '../../components/app-header/app-header';
import AppFooter from '../../components/app-footer/app-footer';
import AppBtnPlay from '../../components/app-btn-play/app-btn-play';
import AppBtnReview from '../../components/app-btn-review/app-btn-review';
import FilmTabs from '../../components/film-tabs/film-tabs';
import commentProp from '../../comment.prop';
import FilmSimilar from '../../components/film-similar/film-similar';

function Film(props) {
  const {film, comments, similar} = props;

  return (
    <React.Fragment>
      <section className="film-card film-card--full">
        <div className="film-card__hero">
          <div className="film-card__bg">
            <img src={film.backgroundImage} alt={film.name}/>
          </div>

          <h1 className="visually-hidden">WTW</h1>

          <AppHeader className='film-card__head' />

          <div className="film-card__wrap">
            <div className="film-card__desc">
              <h2 className="film-card__title">{film.name}</h2>
              <p className="film-card__meta">
                <span className="film-card__genre">{film.genre}</span>
                <span className="film-card__year">{film.released}</span>
              </p>
              <div className="film-card__buttons">
                <AppBtnPlay film={film} />
                <AppBtnReview film={film} />
                <Link to={`${AppRoute.FILM}/${film.id}${AppRoute.ADD_REVIEW}`} className="btn film-card__button">Add review</Link>
              </div>
            </div>
          </div>
        </div>

        <div className="film-card__wrap film-card__translate-top">
          <div className="film-card__info">
            <div className="film-card__poster film-card__poster--big">
              <img src={film.posterImage} alt={film.name} width="218" height="327"/>
            </div>

            <FilmTabs film={film} comments={comments} />

          </div>
        </div>
      </section>

      <div className="page-content">

        <FilmSimilar similar={similar} />
        <AppFooter />

      </div>
    </React.Fragment>
  );
}

Film.propTypes = {
  film: filmProp,
  comments: PropTypes.arrayOf(commentProp),
  similar: PropTypes.arrayOf(filmProp),
};

export default Film;
