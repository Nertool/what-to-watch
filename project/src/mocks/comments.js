const comments = [{
  'id': 1,
  'user': {'id': 15, 'name': 'Kendall'},
  'rating': 9.8,
  'comment': 'The editing is a mess, and the transitions of the plot or characters are rather strange. There is no narrative focus and the storytelling is unbalanced. I cannot really understand why such a bad movie received an overwhelming approval from the critics. ',
  'date': '2021-07-09T19:01:42.859Z',
}, {
  'id': 2,
  'user': {'id': 11, 'name': 'Jack'},
  'rating': 2,
  'comment': 'A movie that will take you to another world full of emotions.',
  'date': '2021-08-01T19:01:42.859Z',
}, {
  'id': 3,
  'user': {'id': 14, 'name': 'Corey'},
  'rating': 2.9,
  'comment': 'The editing is a mess, and the transitions of the plot or characters are rather strange. There is no narrative focus and the storytelling is unbalanced. I cannot really understand why such a bad movie received an overwhelming approval from the critics. ',
  'date': '2021-07-22T19:01:42.859Z',
}, {
  'id': 4,
  'user': {'id': 11, 'name': 'Jack'},
  'rating': 2,
  'comment': 'A movie that will take you to another world full of emotions.',
  'date': '2021-08-01T19:01:42.859Z',
}, {
  'id': 5,
  'user': {'id': 14, 'name': 'Corey'},
  'rating': 2.9,
  'comment': 'The editing is a mess, and the transitions of the plot or characters are rather strange. There is no narrative focus and the storytelling is unbalanced. I cannot really understand why such a bad movie received an overwhelming approval from the critics. ',
  'date': '2021-07-22T19:01:42.859Z',
}];

export default comments;
