import {formatJSON} from '../utils/format-json';

const film = {
  'name': 'Bronson',
  'poster_image': 'https://7.react.pages.academy/static/film/poster/bronson.jpg',
  'preview_image': 'https://7.react.pages.academy/static/film/preview/bronson.jpg',
  'background_image': 'https://7.react.pages.academy/static/film/background/bronson.jpg',
  'background_color': '#73B39A',
  'description': 'A young man who was sentenced to seven years in prison for robbing a post office ends up spending three decades in solitary confinement. During this time, his own personality is supplanted by his alter-ego, Charles Bronson.',
  'rating': 3.6,
  'scores_count': 109661,
  'director': 'Nicolas Winding Refn',
  'starring': ['Tom Hardy', 'Kelly Adams', 'Luing Andrews'],
  'run_time': 92,
  'genre': 'Action',
  'released': 2008,
  'id': 1,
  'is_favorite': false,
  'video_link': 'http://peach.themazzone.com/durian/movies/sintel-1024-surround.mp4',
  'preview_video_link': 'https://upload.wikimedia.org/wikipedia/commons/transcoded/b/b3/Big_Buck_Bunny_Trailer_400p.ogv/Big_Buck_Bunny_Trailer_400p.ogv.360p.webm',
};

export default formatJSON(film);
