import {formatJSON} from '../utils/format-json';

const promo = {
  'name': 'Pulp Fiction',
  'poster_image': 'https://7.react.pages.academy/static/film/poster/Pulp_Fiction.jpg',
  'preview_image': 'https://7.react.pages.academy/static/film/preview/pulp-fiction.jpg',
  'background_image': 'https://7.react.pages.academy/static/film/background/Pulp_Fiction.jpg',
  'background_color': '#795433',
  'description': 'The lives of two mob hitmen, a boxer, a gangster & his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.',
  'rating': 1.5,
  'scores_count': 1635992,
  'director': 'Quentin Tarantino',
  'starring': ['John Travolta', 'Uma Thurman', 'Samuel L. Jackson'],
  'run_time': 153,
  'genre': 'Crime',
  'released': 1994,
  'id': 1,
  'is_favorite': false,
  'video_link': 'http://media.xiph.org/mango/tears_of_steel_1080p.webm',
  'preview_video_link': 'https://upload.wikimedia.org/wikipedia/commons/transcoded/b/b3/Big_Buck_Bunny_Trailer_400p.ogv/Big_Buck_Bunny_Trailer_400p.ogv.360p.webm',
};

export default formatJSON(promo);
