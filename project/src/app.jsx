import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import {AppRoute, AuthStatus} from './const';
import Main from './pages/main/main';
import Login from './pages/login/login';
import AddReview from './pages/add-review/add-review';
import Film from './pages/film/film';
import MyList from './pages/my-list/my-list';
import Player from './pages/player/player';
import NotFound from './pages/not-found/not-found';
import filmProp from './film.prop';
import commentProp from './comment.prop';
import {connect} from 'react-redux';
import {checkAuth} from './store/api-actions';

function App(props) {
  const {film, comments, similar, checkAuthStatus, authStatus} = props;

  useEffect(() => {
    checkAuthStatus();
  }, []);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={AppRoute.ROOT}>
          <Main />
        </Route>
        <Route exact path={AppRoute.LOGIN}>
          { authStatus !== AuthStatus.AUTH ? <Login /> : <Redirect to={AppRoute.ROOT} /> }
        </Route>
        <Route exact path={`${AppRoute.FILM}/:id${AppRoute.ADD_REVIEW}`}>
          <AddReview film={film} />
        </Route>
        <Route exact path={`${AppRoute.FILM}/:id`}>
          <Film film={film} comments={comments} similar={similar} />
        </Route>
        <Route exact path={AppRoute.MY_LIST}>
          <MyList />
        </Route>
        <Route exact path={`${AppRoute.PLAYER}/:id`}>
          <Player />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

App.propTypes = {
  film: filmProp,
  comments: PropTypes.arrayOf(commentProp),
  similar: PropTypes.arrayOf(filmProp),
  checkAuthStatus: PropTypes.func.isRequired,
  authStatus: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  authStatus: state.authStatus,
});

const mapDispatchToProps = (dispatch) => ({
  checkAuthStatus () {
    dispatch(checkAuth());
  },
});

export {App};
export default connect(mapStateToProps, mapDispatchToProps)(App);
